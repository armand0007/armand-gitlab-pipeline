FROM alpine:3

USER armand0007
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "curl --fail http://localhost || exit 1" ]